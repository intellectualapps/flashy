import 'dart:async';
import 'package:flutter/material.dart';
import 'package:zakatify/views/sign_up_screen.dart';
import 'package:zakatify/strings/strings.dart' as string;

void main() {
  runApp(new MaterialApp(
    initialRoute: '/',
    routes: <String, WidgetBuilder>{
      '/': (context) => MainApp(),
      '/welcome': (context) => WelcomeScreen()
    },
    debugShowCheckedModeBanner: false,
  ));
}

class MainApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainAppState();
  }
}

class MainAppState extends State<MainApp> {
  startTime() async {
    var _duration = new Duration(seconds: 9);
    return new Timer(_duration, navigationPage);
  }

  startTimeForLogo() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, startLogo);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacementNamed('/welcome');
  }

  void startLogo() {
    setState(() {
      _toggle = !_toggle;
    });
  }

  @override
  void initState() {
    super.initState();
    startTimeForLogo();
    startTime();
  }

  bool _toggle = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Container(
        padding: new EdgeInsets.only(left: 50.0, right: 50.0),
        height: 300.0,
        child: Column(
          children: <Widget>[
            Expanded(
                child: AnimatedOpacity(
              opacity: _toggle ? 0.0 : 1.0,
              duration: Duration(seconds: 1),
              child: Image.asset(
                "images/zakatify_icon.png",
                width: 100.0,
                height: 100.0,
              ),
            )),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                string.splashLabel,
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff494645)),
                softWrap: true,
                textAlign: TextAlign.center,
              ),
            )),
          ],
        ),
      )),
    );
  }
}
